<!--.page -->
<div role="document" class="page">

<!--.l-header region -->
<header role="banner" class="l-header">

    <?php if ($top_bar): ?>
      <!--.top-bar -->
      <?php if ($top_bar_classes): ?>
      <div class="<?php print $top_bar_classes; ?>">
      <?php endif; ?>
        <nav class="top-bar"<?php print $top_bar_options; ?>>
          <ul class="title-area">
            <li class="name"><h1><?php print $linked_site_name; ?></h1></li>
            <li class="toggle-topbar menu-icon"><a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
          </ul>
          <section class="top-bar-section">
            <?php if ($top_bar_main_menu) :?>
              <?php print $top_bar_main_menu; ?>
            <?php endif; ?>
            <?php if ($top_bar_secondary_menu) :?>
              <?php print $top_bar_secondary_menu; ?>
            <?php endif; ?>
          </section>
        </nav>
      <?php if ($top_bar_classes): ?>
      </div>
      <?php endif; ?>
      <!--/.top-bar -->
    <?php endif; ?>



<section class="row">

    <!-- Title, slogan and menu -->
    <?php if ($alt_header): ?>
    <div class="<?php print $alt_header_classes; ?>">
   
    <div class="large-3 columns">
      <?php if ($linked_logo): print $linked_logo; endif; ?>

      <?php if ($site_name): ?>
        <?php if ($title): ?>
          <div id="site-name" class="element-invisible">
            <strong>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </strong>
          </div>
        <?php else: /* Use h1 when the content title is empty */ ?>
          <h1 id="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
          </h1>
        <?php endif; ?>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <h2 title="<?php print $site_slogan; ?>" class="site-slogan"><?php print $site_slogan; ?></h2>
      <?php endif; ?>

      <?php if ($alt_main_menu): ?>
        <nav id="main-menu" class="navigation" role="navigation">
          <?php print ($alt_main_menu); ?>
        </nav> <!-- /#main-menu -->
      <?php endif; ?>

      <?php if ($alt_secondary_menu): ?>
        <nav id="secondary-menu" class="navigation" role="navigation">
          <?php print $alt_secondary_menu; ?>
        </nav> <!-- /#secondary-menu -->
      <?php endif; ?>


    </div>
    <?php endif; ?>
    <!-- End title, slogan and menu -->


    <?php if (!empty($page['header'])): ?>
      <!--.l-header-region -->
      <div class="l-header-region">
        <div class="large-9 columns">
          <?php print render($page['header']); ?>
        </div>
      </div>
      <!--/.l-header-region -->
    <?php endif; ?>

</section>
</header>
<!--/.l-header -->
    
    

<!-- Robonation TV feature area -->    
<div id="feature">
  <?php if (!empty($page['featured'])): ?>
   
    <!--/.featured -->
    <section class="l-featured row">

      <div class="large-8 columns">
        <!-- feature video-->
        <?php $block = module_invoke('block', 'block_view', '19'); echo $block['content']; ?>
      </div>

      <div class="feature-sidebar large-4 columns">
        <!-- feature buttons-->
        <div class="feature-buttons">
          <?php $block = module_invoke('block', 'block_view', '34'); echo $block['content']; ?>
        </div>
        <div class="feature-testimonials">
          <!-- feature testimonials-->
          <?php print views_embed_view('testimonials', 'block'); ?>
        </div>
      </div>

    </section>
    <!--/.l-featured -->

  <?php endif; ?>
</div>


    
<!-- drupal messages and alerts region -->
  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--/.l-messages -->
    <section class="l-messages row">
      <div class="large-12 columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </section>
    <!--/.l-messages -->
  <?php endif; ?>

  <?php if (!empty($page['help'])): ?>
    <!--/.l-help -->
    <section class="l-help row">
      <div class="large-12 columns">
        <?php print render($page['help']); ?>
      </div>
    </section>
    <!--/.l-help -->
  <?php endif; ?>
    
    
    

<!-- competitions -->
    <div id="homepage-competitions">
    <section class="row">
      <div class="large-12 columns">
        <h2>Competitions</h2>
        <?php $block = module_invoke('quicktabs', 'block_view', 'competitions'); print render($block['content']); ?>
      </div>
    </section>
    </div>

<!-- programs -->
    <div id="homepage-programs">
    <section class="row">
      <div class="large-12 columns">
        <h2>Programs</h2>
          <?php $block = module_invoke('quicktabs', 'block_view', 'programs'); print render($block['content']); ?>
      </div>
    </section>
    </div>

<!-- get involved -->
    <div id="homepage-get-involved">
    <section class="row">
      <div class="small-12 medium-6 columns">
        <h2>Get Involved</h2>
          <?php $block = module_invoke('block', 'block_view', '15'); echo $block['content']; ?>
      </div>
      <div class="small-12 medium-6 columns">
        <h2>What's Happening Near You</h2>
          <?php $block = module_invoke('block', 'block_view', '31'); echo $block['content']; ?>
      </div>
    </section>
    </div>

    
    
    
    
<!--  
  <main role="main" class="row l-main">

    <div class="main large-12 columns">
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlight panel callout">
          <?php print render($page['highlighted']); ?>
        </div>
      <?php endif; ?>

      <a id="main-content"></a>

      <?php if ($breadcrumb): print $breadcrumb; endif; ?>

      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
        <?php if (!empty($tabs2)): print render($tabs2); endif; ?>
      <?php endif; ?>

      <?php if ($action_links): ?>
        <ul class="action-links">
          <?php print render($action_links); ?>
        </ul>
      <?php endif; ?>

      <?php print render($page['content']); ?>
    </div>

  </main>
-->






  <!--CALLOUTS-->
  <?php if (!empty($page['triptych_first']) || !empty($page['triptych_middle']) || !empty($page['triptych_last'])): ?>
    <!--.triptych-->
    <section class="l-triptych row">
      <div class="triptych-first large-4 columns">
        <?php print render($page['triptych_first']); ?>
      </div>
      <div class="triptych-middle large-4 columns">
        <?php print render($page['triptych_middle']); ?>
      </div>
      <div class="triptych-last large-4 columns">
        <?php print render($page['triptych_last']); ?>
      </div>
    </section>
    <!--/.triptych -->
  <?php endif; ?>


  <div class="sponsors">
    <?php if (!empty($page['footer_firstcolumn']) || !empty($page['footer_secondcolumn']) || !empty($page['footer_thirdcolumn']) || !empty($page['footer_fourthcolumn'])): ?>
      <!--.footer-columns -->
      <section class="row l-footer-columns">
        <?php if (!empty($page['footer_firstcolumn'])): ?>
          <div class="footer-first large-12 columns">
            <?php print render($page['footer_firstcolumn']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($page['footer_secondcolumn'])): ?>
          <div class="footer-second large-12 columns">
            <?php print render($page['footer_secondcolumn']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($page['footer_thirdcolumn'])): ?>
          <div class="footer-third large-12 columns">
            <?php print render($page['footer_thirdcolumn']); ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($page['footer_fourthcolumn'])): ?>
          <div class="footer-fourth large-12 columns">
            <?php print render($page['footer_fourthcolumn']); ?>
          </div>
        <?php endif; ?>
      </section>
      <!--/.footer-columns-->
    <?php endif; ?>
  </div>

  <!--.l-footer-->
  <div class="main-footer">
    <footer class="l-footer panel row" role="contentinfo">
      <?php if (!empty($page['footer'])): ?>
        <div class="footer large-12 columns">
          <?php print render($page['footer']); ?>
        </div>
      <?php endif; ?>

      <?php if ($site_name) :?>
        <div class="copyright large-12 columns">
          &copy; <?php print date('Y') . ' ' . check_plain($site_name) . ' ' . t('All rights reserved.'); ?>
        </div>
      <?php endif; ?>
    </footer>
  </div>
  <!--/.footer-->

  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->
