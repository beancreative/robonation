(function ($, Drupal) {

  Drupal.behaviors.STARTER = {
    attach: function(context, settings) {	

		var x = $('.quicktabs-tabs'); 
		var w = $(window).width(); 
		if(w <= 768) {
			x.addClass('mobile-slider');
		}
		else if(w >= 769){
			x.removeClass('mobile-slider');
		}


	    $('.mobile-slider').bxSlider({
	        mode: 'fade',
	        pager: false
	    });


    }
  };
})(jQuery, Drupal);
 